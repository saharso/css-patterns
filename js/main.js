//@ts-check
"use strict";
//@ts-ignore
const $ = window.jQuery;
const chartsMap = new Map();
class MakeWidget {
    constructor( appendTo ){
        this.appendTo = appendTo;
        this._initWidgetSturcture();
    }
    widget = document.createElement('section');
    header = document.createElement('header');
    main = document.createElement('main');

    _initWidgetSturcture(){
        this.header.className = 'l-align-verticle b-widget-header';
        this.header.innerHTML = `
            <div class="l-cellSpread-x t-marginEnd-med t-titleHold"></div>
            <div class="b-widget-panel"></div>
        `;
        this.widget.className = 'b-widget';
        this.widget.insertBefore( this.main, this.widget.firstChild );
        this.widget.insertBefore( this.header, this.widget.firstChild );
    }

    buildWidgetTitle(title, titleLevel = 2){
        this.header.querySelector('.t-titleHold').innerHTML = `
            <h${titleLevel} class="t-title">
                ${title}
            </h${titleLevel}>
        `;
        return this;
    }

    buildWidgetHTML( htmlString ){
        this.main.innerHTML = `
            ${htmlString}
        `;
        return this;
    }

    buildWidgetJS( jsFunction ){
        if( typeof jsFunction === 'function' ){
            requestAnimationFrame( ()=> jsFunction(this.widget) );
        }
        return this;
    }

    buildWidgetClassList( className ){
        this.appendTo.appendChild( this.widget );
        if( className ){
            this.widget.classList.add( ...className.split(' ') );
        }
        return this;
    }

}

(()=>{
    const mainPanel = document.getElementById('mainPanel');
    const sidebar = document.getElementById('mainSidebar');

    new MakeWidget( mainPanel ).
    buildWidgetHTML(
        `
        <div class="l-align-verticle">
            <h3 class="l-cellSpread-x t-row-0 t-title">
                Controllers
            </h3>
            <button class="t-marginStart-small b-controller">
                <i class="fas fa-filter"></i>
            </button>
            <button class="t-marginStart-small b-controller">
                <i class="fas fa-trash-alt"></i>
            </button>
            <button class="t-marginStart-small b-controller">
                <i class="fas fa-download"></i>
            </button>
        </div>

        `
    ).buildWidgetClassList('l-span-5');

    new MakeWidget( mainPanel ).
    buildWidgetTitle('Plain Text Example').
    buildWidgetHTML(
        `
            <h3 class="t-title fs-20">
                An Example of a h3 title
            </h3>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                galley of type and scrambled it to make a type specimen book. It has survived not only.
            </p>
            <h4>Smaller Title</h4>
            <p>
                five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </p>
        `
    ).buildWidgetClassList('l-span-2');

    new MakeWidget( mainPanel ).
    buildWidgetHTML(
        `
        <div class="t-row-med b-tableWrap">
            <table class="data">
            <thead>
                <tr>
                    <th>Entry Header 1</th>
                    <th>Entry Header 2</th>
                    <th>Entry Header 3</th>
                    <th>Entry Header 4</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Entry First Line 1</td>
                    <td>Entry First Line 2</td>
                    <td>Entry First Line 3</td>
                    <td>Entry First Line 4</td>
                </tr>
                <tr>
                    <td>Entry Line 1</td>
                    <td>Entry Line 2</td>
                    <td>Entry Line 3</td>
                    <td>Entry Line 4</td>
                </tr>
                <tr>
                    <td>Entry Last Line 1</td>
                    <td>Entry Last Line 2</td>
                    <td>Entry Last Line 3</td>
                    <td>Entry Last Line 4</td>
                </tr>
            </tbody>
            </table>    
        </div>    
        <div class="t-row-med b-tableWrap">
            <table class="data">
            <thead>
                <tr>
                    <th>Entry Header 1</th>
                    <th>Entry Header 2</th>
                    <th>Entry Header 3</th>
                    <th>Entry Header 4</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Entry First Line 1</td>
                    <td>Entry First Line 2</td>
                    <td>Entry First Line 3</td>
                    <td>Entry First Line 4</td>
                </tr>
                <tr>
                    <td>Entry Line 1</td>
                    <td>Entry Line 2</td>
                    <td>Entry Line 3</td>
                    <td>Entry Line 4</td>
                </tr>
                <tr>
                    <td>Entry Last Line 1</td>
                    <td>Entry Last Line 2</td>
                    <td>Entry Last Line 3</td>
                    <td>Entry Last Line 4</td>
                </tr>
            </tbody>
            </table>    
        </div>    

        `
    ).buildWidgetClassList('l-span-3');

    new MakeWidget( mainPanel ).
    buildWidgetTitle('An example with bar chart').
    buildWidgetHTML(
        `
            HI!
            <canvas id="myChart" width="400" height="400"></canvas>
        `
    ).
    buildWidgetJS(
        ()=>{
            // @ts-ignore
            var ctx = document.getElementById('myChart').getContext('2d');
            chartsMap.set( 
                'barChart',
                // @ts-ignore
                new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                        datasets: [{
                            label: 'Number of Votes',
                            data: [12, 15, 3, 5, 2, 3],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                })
            );
        },
    ).
    buildWidgetClassList('l-span-3');

    new MakeWidget( mainPanel ).
    buildWidgetTitle('Pie chart example').
    buildWidgetHTML(
        `
            <div class="t-row-med">
                <canvas id="pieChart" width="400" height="400"></canvas>
            </div>
            <div class="b-widget">
                <h3>
                    Lorem Ipsum is simply
                </h3>
                <p>
                    Dummy text of the printing and typesetting industry. Lorem Ipsum has
                    been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book. It has survived not only.
                </p>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                    been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book. It has survived not only.
                </p>

            </div>
        `
    ).buildWidgetJS(
        ()=>{
            // @ts-ignore
            var ctx = document.getElementById('pieChart').getContext('2d');
            chartsMap.set( 
                'pieChart',
                // @ts-ignore
                new Chart(ctx, {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [10, 20, 30],
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
        
                        }],
                        labels: [
                            'Red',
                            'Yellow',
                            'Blue'
                        ],
                    },
                })
            );
        },
    ).buildWidgetClassList('l-span-2');

    new MakeWidget( mainPanel ).
    buildWidgetTitle('An Example Form').
    buildWidgetHTML(
        `
        <form action="#" method="post" class="b-submitDetails">
            <div class="l-grid t-row-med">
                <label class="l-align-verticle t-label" for="name">Name:</label>
                <input class="b-input" type="text" id="name" placeholder="Enter your full name" />
            
                <label class="l-align-verticle t-label" for="email">Email:</label>
                <input class="b-input" type="email" id="email" placeholder="Enter your email address" />
            
                <label class="t-label" for="message">Message:</label>
                <textarea class="b-input b-input--textArea" id="message" placeholder="What's on your mind?"></textarea>
            </div>

            <div class="l-align-end">
                <button class="b-button" type="submit">
                    Send message
                    <i class="fas fa-chevron-right"></i>
                </button>
            </div>
        
        </form>        
        `
    ).buildWidgetClassList('l-span-3');

    new MakeWidget( mainPanel ).
    buildWidgetTitle('A Slider example').
    buildWidgetHTML(
        `<div id="sliderExample" class="b-slider"></div>`,
    ).
    buildWidgetJS(
        ()=>{
            const slider = document.getElementById('sliderExample');
            [
                'assets/images/01.jpg',
                'assets/images/02.jpg',
                'assets/images/03.jpg',
            ].forEach((imgUrl)=>{
                const slide = document.createElement('div');
                slide.className = 'b-slider-slide';
                slide.innerHTML = `
                    <div class="l-h-full l-align-verticle"><img src="${imgUrl}" alt=""/></div>
                `;
                slider.appendChild(slide);
            });
            $('#sliderExample').slick({
                prevArrow: `
                    <button class="t-marginEnd-med t-hover slider-btn is-prev">
                        <i class="fas fa-chevron-left"></i>
                    </button>
                    `,
                nextArrow: `
                    <button class="t-marginStart-med t-hover slider-btn is-next">
                        <i class="fas fa-chevron-right"></i>
                    </button>
                    `,
            });
        }
    ).buildWidgetClassList('l-span-2');

    new MakeWidget( mainPanel ).
    buildWidgetHTML(
        `
        <div class="b-stacked">
            <div style="min-width: 700px">
                <div class="t-row-med">
                    <canvas id="stacked"></canvas>
                </div>

                <div class="l-flex l-flex-wrap">
                    <button class="b-button t-marginStart-small t-bg-purple" id="randomizeData">Randomize Data</button>
                    <button class="b-button t-marginStart-small t-bg-magenta" id="addDataset">Add Dataset</button>
                    <button class="b-button t-marginStart-small t-bg-lime" id="removeDataset">Remove Dataset</button>
                    <button class="b-button t-marginStart-small t-bg-cyanLight" id="addData">Add Data</button>
                    <button class="b-button t-marginStart-small" id="removeData">Remove Data</button>
                </div>
            </div>
        </div>
        `
    ).
    buildWidgetJS(
        ()=>{
            const randomScalingFactor = function() {
                //@ts-ignore
                return Math.round(Samples.utils.rand(-100, 100));
            };          
            const chartColors = {
                red: 'rgb(255, 99, 132, 0.3)',
                orange: 'rgb(255, 159, 64, 0.3)',
                yellow: 'rgb(255, 205, 86, 0.3)',
                green: 'rgb(75, 192, 192, 0.3)',
                blue: 'rgb(54, 162, 235, 0.3)',
                purple: 'rgb(153, 102, 255, 0.3)',
                grey: 'rgb(201, 203, 207, 0.3)'
            };
            var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var config = {
                type: 'line',
                data: {
                    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                    datasets: [{
                        label: 'My First dataset',
                        borderColor: chartColors.red,
                        backgroundColor: chartColors.red,
                        data: [
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor()
                        ],
                    }, {
                        label: 'My Second dataset',
                        borderColor: chartColors.blue,
                        backgroundColor: chartColors.blue,
                        data: [
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor()
                        ],
                    }, {
                        label: 'My Third dataset',
                        borderColor: chartColors.green,
                        backgroundColor: chartColors.green,
                        data: [
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor()
                        ],
                    }, {
                        label: 'My Third dataset',
                        borderColor: chartColors.yellow,
                        backgroundColor: chartColors.yellow,
                        data: [
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor(),
                            randomScalingFactor()
                        ],
                    }]
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: 'Chart.js Line Chart - Stacked Area'
                    },
                    tooltips: {
                        mode: 'index',
                    },
                    hover: {
                        mode: 'index'
                    },
                    scales: {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            }
                        }]
                    }
                }
            };
    
            //@ts-ignore
            var ctx = document.getElementById('stacked').getContext('2d');
            
            //@ts-ignore
            chartsMap.set('stacked', new Chart(ctx, config) );
    
            document.getElementById('randomizeData').addEventListener('click', function() {
                config.data.datasets.forEach(function(dataset) {
                    dataset.data = dataset.data.map(function() {
                        return randomScalingFactor();
                    });
    
                });
    
                chartsMap.get('stacked').update();
            });
    
            var colorNames = Object.keys(chartColors);
            document.getElementById('addDataset').addEventListener('click', function() {
                var colorName = colorNames[config.data.datasets.length % colorNames.length];
                var newColor = chartColors[colorName];
                var newDataset = {
                    label: 'Dataset ' + config.data.datasets.length,
                    borderColor: newColor,
                    backgroundColor: newColor,
                    data: [],
                };
    
                for (var index = 0; index < config.data.labels.length; ++index) {
                    newDataset.data.push(randomScalingFactor());
                }
    
                config.data.datasets.push(newDataset);
                chartsMap.get('stacked').update();
            });
    
            document.getElementById('addData').addEventListener('click', function() {
                if (config.data.datasets.length > 0) {
                    var month = MONTHS[config.data.labels.length % MONTHS.length];
                    config.data.labels.push(month);
    
                    config.data.datasets.forEach(function(dataset) {
                        dataset.data.push(randomScalingFactor());
                    });
    
                    chartsMap.get('stacked').update();
                }
            });
    
            document.getElementById('removeDataset').addEventListener('click', function() {
                config.data.datasets.splice(0, 1);
                chartsMap.get('stacked').update();
            });
    
            document.getElementById('removeData').addEventListener('click', function() {
                config.data.labels.splice(-1, 1); // remove the label first
    
                config.data.datasets.forEach(function(dataset) {
                    dataset.data.pop();
                });
    
                chartsMap.get('stacked').update();
            });
        }
    ).buildWidgetClassList('l-span-5');

    new MakeWidget( mainPanel ).
    buildWidgetHTML(
        `
        <div class="b-iframe">
            <iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQAXU1EyJ4WTwNh_d-fRMTe7NR-JuOykIi3fTcJDrXfOGNdnOxqXrADT9MQqK2ZEwDGFt8h5IV0kxL7/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>        
        </div>
        `
    ).buildWidgetClassList('l-span-5');

    new MakeWidget( mainPanel ).
    buildWidgetHTML(
        `Some bottom disclaimer`,
    ).buildWidgetClassList('l-span-5 l-align-end t-fs-small b-widtet--stripped');


    // sidebar 

    new MakeWidget( sidebar ).
    buildWidgetTitle('Sidebar').
    buildWidgetJS(
        (widget)=>{
            const ul = document.createElement('ul');
            ul.className = 'norm-ul t-list';
            const createLis = (ul,content = '',hasArrow = true)=>{
                const li = document.createElement('li');
                let arrow = '';
                if(hasArrow){
                    arrow = `<i class="fas fa-arrow-alt-circle-right t-marginEnd-small t-c-main"></i>`;
                }
                li.innerHTML = `
                <div class="l-align-top t-row-small">
                    ${arrow}
                    <div class="l-cellSpread-x">
                        <a href="#" class="norm-a t-hover">${content}</a>
                    </div>
                </div>`;

                ul.appendChild(li);
            }
            widget.appendChild(ul);
            [
                `<b>File distribution</b>`,
                [
                    `You know where everything is`,
                    `No more deep nestings`,
                    `No more !importants`,
                ],
                `<b>Maintainability</b>`,
                [
                    `Much less code`,
                    `No mater how large your app is, css will remain relatively small`,
                    `Provides guidelines and rules`,
                ],
                `<b>Time saving</b>`,
                [
                    `Responsive design becomes easy`,
                    `Create highly complex layouts only with HTML`,
                ]
            ].forEach( item => {
                if( typeof item === 'string' ){
                    createLis(ul, item, false);
                } else {
                    const subUl = document.createElement('ul');
                    subUl.className = 'norm-ul t-list';
                    item.forEach((subItem)=>{
                        createLis(subUl, subItem);
                    });
                    ul.lastElementChild.appendChild(subUl);
                }
            });

        }
    ).buildWidgetClassList('b-widget--nullify');

})();

const toggleSidebarView = (()=>{
    const button = document.getElementById('sidebarToggle');
    const sidebar = document.getElementById('mainSidebar');
    let cond = false;
    button.addEventListener('click',()=>{
        cond = ! cond;
        let classAction = cond ? 'add' : 'remove';
        sidebar.classList[classAction]('is-toggled');
        if( document.body.scrollWidth > 1000 ){
            const interval = setInterval(()=>{
                chartsMap.forEach( (chart)=>{
                    chart.resize();
                });

            },10);
            setTimeout(()=>{
                $('#sliderExample').slick('refresh');
                clearInterval(interval);
            }
            ,400);
        }
    })
})();

const moveMainMenuToSidebarOnMobile = (()=>{
    const mqMobile = 1000;
    const mainMenu = document.getElementById('mainMenu');
    const desktopLocation = mainMenu.parentNode;
    const mobileLocation = document.getElementById('mainSidebar');
    const make = ()=>{
        if( document.body.scrollWidth <= mqMobile ){
            mobileLocation.insertBefore(mainMenu, mobileLocation.firstChild);
        } else {
            desktopLocation.appendChild(mainMenu);
        }
    }
    make();
    window.addEventListener('resize', make );
})();

$( "#mainPanel" ).sortable();